<?php

$url = "https://www.salesauto2000medan.com/test-drive/";

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$source = curl_exec($curl);

//potong html jadi tinggal data json dulu
//potong akhir
$temp = @explode(';nfForms.push(form);', $source)[0];
$jsonFields = @explode('form.fields=', $temp)[1];

$fieldsArray = json_decode($jsonFields);

$fieldUntukSelect = null;
foreach ($fieldsArray as $field) {
    if ($field->type == "listselect") {
        $fieldUntukSelect = $field;
        break;
    }
}



$result = [];
if ($fieldUntukSelect) {
    foreach ($fieldUntukSelect->options as $optionItem) {
        $result[] = $optionItem->value;
    }
}
header('Content-Type: application/json');
echo json_encode($result);
