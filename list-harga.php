<?php

include 'php-selector/selector.inc';

$url = "https://www.salesauto2000medan.com/list-harga/";

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

$source = curl_exec($curl);

$dom = new SelectorDOM($source);

$imagesDom = $dom->select("article div.entry-content p img");

$result = [];
foreach ($imagesDom as $item) {
    $imgUrl = @$item['attributes']['src'];

    if ($imgUrl) {
        $result[] = $imgUrl;
    }
}

header('Content-Type: application/json');
echo json_encode($result);
